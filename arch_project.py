from operator import itemgetter

def main(path):
    f = open(path, 'r')
    stack_size_1 = 3
    stack_size_2 = 4
    stack_size_3 = 5
    stack_size_4 = 6
    stack_size_5 = 7
    stack_size_6 = 8

    data = []
    stack = []
    for line in f:
        data.append(int(line.strip()))
    
    data = [1,2,3,4,1,2,5,1,2,3,4,5]

    fifo(data, stack, stack_size_1)
    fifo(data, stack, stack_size_2)
    # fifo(data, stack, stack_size_3)
    # fifo(data, stack, stack_size_4)
    # fifo(data, stack, stack_size_5)
    # fifo(data, stack, stack_size_6)

    optimal(data, stack, stack_size_1)
    optimal(data, stack, stack_size_2)
    # optimal(data, stack, stack_size_3)
    # optimal(data, stack, stack_size_4)
    # optimal(data, stack, stack_size_5)
    # optimal(data, stack, stack_size_6)

    lfu(data, stack, stack_size_1)
    lfu(data, stack, stack_size_2)
    # lfu(data, stack, stack_size_3)
    # lfu(data, stack, stack_size_4)
    # lfu(data, stack, stack_size_5)
    # lfu(data, stack, stack_size_6)

    made_up(data, stack, stack_size_1)
    made_up(data, stack, stack_size_2)
    # made_up(data, stack, stack_size_3)
    # made_up(data, stack, stack_size_4)
    # made_up(data, stack, stack_size_5)
    # made_up(data, stack, stack_size_6)
    
    LRU(data, stack_size_2)


######################### FIRST IN FIRST OUT  #####################
def fifo(data, stack, num_states):
    print ("FIFO", num_states, " frames")
    print ("...........................................")
    miss = 0
    stack = []
    #replaced the oldest used page
    for i in range (0, num_states):
        stack.append(data[i])
        miss += 1
        print (data[i], "    ",stack)
    for i in range(num_states, len(data)):
        if data[i] not in stack:
            miss += 1
            for j in range(0, i):
                if data[j] in stack:
                    stack[stack.index(data[j])] = data[i]
                    print (data[i], "    ",stack)
                    break
        else:
            print (data[i])
    print ("..........................................."       )
    print ("First in first out miss for ",num_states, ": ",miss)
##########################  OPTIMAL  ##############################

def optimal(data, stack, num_states):
    print ("OPTIMAL", num_states, " frames"             )
    print ("...........................................")
    miss = 0
    count = 0
    stack = []

    for i in range (0, num_states):
        stack.append(data[i])
        print (data[i], "    ",stack)

    for i in range (num_states, len(data)):
        if data[i] not in stack:
            miss += 1
            arr = data[i:len(data)]
            arr1 = []
            for j in range(0, len(stack)):
                if stack[j] in arr:
                    arr1.append(arr.index(stack[j]))
            max_dist = max(arr1)                  #get the page farthest away
            stack[arr1.index(max_dist)] = data[i]       #replace the farthest page
            print (data[i], "    ",stack)
        else:     
            print (data[i])
    print ("optimal miss rate for ",num_states, ": ",miss)
    print ("..........................................." )
######################## Least Frequently Used  ###################

def lfu(data, stack, num_states):
    print ("LFU", num_states, " frames"                 )
    print ("...........................................")
    miss  = 0
    count = 0
    size_map = max(data)
    map = []
    stack = []

    for i in range(0, size_map):
        map.append(0)

    for i in range (0, num_states):
        stack.append(data[i])
        print (data[i], "    ",stack)
        map[data[i]-1] += 1
    for i in range(num_states, len(data)):
        if data[i] not in stack:
            miss += 1
            arr = []
            for x in stack:
                arr.append(map[x-1])
            least_recent = min(arr)
            if arr.count(least_recent) > 1:
                conflict_elements = []
                for x1 in range (0, len(arr)):
                    if arr[x1] == least_recent:
                        conflict_elements.append(stack[x1])
                arr1 = []
                for x1 in conflict_elements:
                    arr1.append(data.index(x1))
                f_in = min(arr1)
                stack[arr1.index(f_in)] = data[i]
                print (data[i], "    ",stack)
            else:
                stack[arr.index(least_recent)] = data[i]
                print (data[i], "    ",stack)
        else:
            print (data[i])

    print ("lfu miss rate for ",num_states, ": ",miss   )
    print ("...........................................")

#####################  Made Up Algorithm  #########################

def made_up(data, stack, num_states):
    #setting up a map to store number if times each value is repeated
    print ("...........................................")

    miss = 0
    size_map = max(data)
    map = []
    stack = []
    for i in range(0, num_states):
        stack.append(data[i])
    for i in range(0, size_map):
        map.append(0)
    for i in data:
        map[i-1] += 1
    #start iterating trough data
    for i in range(2, len(data)):
        for j in data:
            if data[i] not in stack:
                miss += 1
                arr = []
                for k in stack:
                    arr.append(map[k-1])
                least_recent = min(arr)
                stack[arr.index(least_recent)] = data[i]
               # print stack

    print ("Made up algorithm rate for ",num_states, " : ",miss)
    print ("..........................................."       )

#---------------------------------------------------------------------
def LRU(data, num_states):
    stack = []
    found = False
    count_miss = 0
    for time_stmp, a_val in enumerate(data):
        for idx, a_tuple in enumerate(stack):
            if a_tuple[1] == a_val:
                stack[idx][0] = time_stmp
                found = True
                break
        if found:
            found = False
            continue
        if len(stack)<num_states: stack.append([time_stmp,a_val])
        else: 
            least_idx = tack.index(min(stack,key=itemgetter(0)))
            stack[least_idx] = [time_stmp,a_val]
            count_miss += 1
#---------------------------------------------------------------------



main("project_input.txt")
